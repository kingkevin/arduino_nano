/* This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <stdint.h> // Standard Integer Types
#include <stdio.h> // Standard IO facilities
#include <stdlib.h> // General utilities
#include <stdbool.h> // Boolean
#include <string.h> // Strings

#include <avr/io.h> // AVR device-specific IO definitions
#include <util/delay.h> // Convenience functions for busy-wait delay loops
#include <avr/interrupt.h> // Interrupts
#include <avr/wdt.h> // Watchdog timer handling
#include <avr/pgmspace.h> // Program Space Utilities
#include <avr/sleep.h> // Power Management and Sleep Modes

#include "usart.h" // basic USART functions
#include "main.h" // main definitions

/* help strings */
const char help_00[] PROGMEM = "commands:\n";
const char help_01[] PROGMEM = "\thelp                  display this help\n";
const char help_02[] PROGMEM = "\tled on|off|toggle     change LED state\n";
const char help_03[] PROGMEM = "\tread [b-dB-D][0-7]       read value from pin\n";
const char help_04[] PROGMEM = "\twrite [b-dB-D][0-7] 0|1  write value on pin\n";
const char help_05[] PROGMEM = "\t[b-dB-D][0-7][0|1]    read/write value from/on pin\n";
PGM_P const help_table[] PROGMEM = {
    help_00,
    help_01,
    help_02,
    help_03,
    help_04,
    help_05
};

/* global variables */
char usart_in[32] = {0}; // user input from USART
volatile uint8_t usart_in_i = 0; // how many USART characters have been input

/* flags, set in the interrupts and handled in the main program */
volatile bool usart_flag = false; // USART input data is available

/* switch off LED */
void led_off(void)
{
	LED_PORT &= ~(1<<LED_IO); // remove power to LED
}

/* switch on LED */
void led_on(void)
{
	LED_PORT |= (1<<LED_IO); // provide power to LED
}

/* toggle LED */
void led_toggle(void)
{
	LED_PIN |= (1<<LED_IO);
}

/* write value on pin from port
 * all parameters are ASCII encoded
 * return -1 if failed, else value
 */
int8_t write_pin(char port, char pin, char value) {
	if (((port>='b' && port<='d') || (port>='B' && port<='D')) && pin>='0' && pin<='7' && value>='0' && value<='1') {
		uint8_t ppin = pin-'0';
		volatile uint8_t * pport = NULL;
		volatile uint8_t * ddr = NULL;
		switch (port) {
			case 'b':
			case 'B':
				pport = &PORTB;
				ddr = &DDRB;
				break;
			case 'c':
			case 'C':
				pport = &PORTC;
				ddr = &DDRC;
				break;
			case 'd':
			case 'D':
				pport = &PORTD;
				ddr = &DDRD;
				break;
			default:
				return -1;
				break;
		}
		*ddr |= (1<<ppin); // set as output
		/* write value */
		if ('0'==value) {
			*pport &= ~(1<<ppin);
			return 0;
		} else if ('1'==value) {
			*pport |= (1<<ppin);
			return 1;
		} else {
			return -1;
		}
	} else {
		return -1;
	}
}

/* read value from pin from port
 * all parameters are ASCII encoded
 * return -1 if failed, else value
 */
int8_t read_pin(char port, char pin) {
	if (((port>='b' && port<='d') || (port>='B' && port<='D')) && pin>='0' && pin<='7') {
		uint8_t ppin = pin-'0';
		volatile uint8_t * pport = NULL;
		volatile uint8_t * ddr = NULL;
		switch (port) {
			case 'b':
			case 'B':
				pport = &PINB;
				ddr = &DDRB;
				break;
			case 'c':
			case 'C':
				pport = &PINC;
				ddr = &DDRC;
				break;
			case 'd':
			case 'D':
				pport = &PIND;
				ddr = &DDRD;
				break;
			default:
				return -1;
				break;
		}
		*ddr &= ~(1<<ppin); // set as input
		return ((*pport>>ppin)&0x1);
	} else {
		return -1;
	}
}

void help(void)
{
	char* str;
	for (uint8_t i=0; i<sizeof(help_table)/sizeof(PGM_P); i++) { /* display all help lines */
		str = malloc(strlen_PF((uint_farptr_t)pgm_read_word(&(help_table[i]))));
		strcpy_PF(str, (uint_farptr_t)pgm_read_word(&(help_table[i])));
		printf(str);
		free(str);
	}
}

/* disable watchdog when booting */
void wdt_init(void) __attribute__((naked)) __attribute__((section(".init3")));
void wdt_init(void)
{
	MCUSR = 0;
	wdt_disable();

	return;
}

/* initialize GPIO */
void io_init(void)
{
	/* use USART as terminal */
	usart_init();
	stdout = &usart_output;
	stdin  = &usart_input;

	/* gpio */
	/* LED */
	LED_DDR |= (1<<LED_IO); // LED is driven by pin (set as output)
	led_off();

	sei(); /* enable interrupts */
}

/* process command */
void command_action()
{
	/* split command */
	const char* delimiter = " ";
	char* word = strtok(usart_in,delimiter);
	if (!word) {
		goto error;
	}
	/* parse command */
	if (0==strcmp(word,"help")) {
		help();
	} else if (0==strcmp(word,"led")) {
		/* expecting state */
		word = strtok(NULL,delimiter);
		if (!word) {
			goto error;
		}
		if (0==strcmp(word,"on")) {
			led_on();
		} else if (0==strcmp(word,"off")) {
			led_off();
		} else if (0==strcmp(word,"toggle")) {
			led_toggle();
		} else {
			goto error;
		}
	} else if (0==strcmp(word,"read")) {
		/* expecting pin */
		word = strtok(NULL,delimiter);
		if (!word || strlen(word)!=2) {
			goto error;
		}
		int8_t value = read_pin(word[0],word[1]);
		if (value>=0) {
			printf("%s: %d\n", word, value);
		} else {
			goto error;
		}
	} else if (0==strcmp(word,"write")) {
		/* expecting pin */
		char * pin = strtok(NULL,delimiter);
		if (!pin || strlen(pin)!=2) {
			goto error;
		}
		/* expecting value */
		char* value = strtok(NULL,delimiter);
		if (!value || strlen(value)!=1) {
			goto error;
		}		
		int8_t rc = write_pin(pin[0],pin[1],value[0]);
		if (rc>=0) {
			printf("%s: %s\n", pin, value);
		} else {
			goto error;
		}
	} else if (2==strlen(word)) { // read value
		int8_t value = read_pin(word[0],word[1]);
		if (value>=0) {
			printf("%s: %d\n", word, value);
		} else {
			goto error;
		}
	} else if (3==strlen(word)) { // write value
		int8_t value = write_pin(word[0],word[1],word[2]);
		if (value>=0) {
			printf("%c%c: %d\n", word[0],word[1],value);
		} else {
			goto error;
		}
	} else {
		goto error;
	}

	return;
error:
	printf(PSTR("command not recognized\n"));
	return;
}

int main(void)
{
	uint8_t usart_out_i = 0; // how many UART input characters have been processed

	io_init(); // initialize IOs
	
	printf(PSTR("welcome to the demo code\n"));
	while (true) { // endless loop for micro-controller
		while (usart_flag) { // new UART input
			usart_flag = false;
			while (usart_out_i<usart_in_i) {
				putchar(usart_in[usart_out_i++]); // print current character
			}
			if ('\n'==usart_in[usart_in_i-1] || '\r'==usart_in[usart_in_i-1]) { // end of line detected
				if ('\r'==usart_in[usart_in_i-1]) { // be sure to print a newline (for windows and mac EOL)
					putchar('\n');
				}
				usart_in[usart_in_i-1] = 0; // remove end of line
				if (usart_in_i>1) { // ignore empty lines
					command_action(); // process command
				}
				usart_out_i = 0; // reset output
				usart_in_i = 0; // reset input
			}
		}
		/* go to sleep and wait for next interrupt */
		set_sleep_mode(SLEEP_MODE_IDLE);
		sleep_mode();
	}
	return 0;
}

