/* This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
/* Copyright (c) 2015 King Kévin <kingkevin@cuvoodoo.info> */
/* This library allows to communicate with a nordic semiconductor nRF24L01 2.4GHz single chip transceiver */

/* Chip Enable pin to activate TX/RX */
#define CE_IO PB1
#define CE_PORT PORTB
#define CE_DDR DDRB
#define CE_PIN PINB
/* Interrupt ReQuest pin to activate TX/RX */
// comment out IRQ_IO if you don't want to use interrupts
#define IRQ_IO PD2
#define IRQ_PORT PORTD
#define IRQ_DDR DDRD
#define IRQ_PIN PIND
#define IRQ_PCIE PCIE2
#define IRQ_PCMSK PCMSK2
#define IRQ_PCINT_vect PCINT2_vect
// if you use PD2 or PD3 you can use the INT0 or INT1 interrupt so to not take over a whole PORT PCINT interrupt
#if defined(IRQ_IO) && ((IRQ_IO==PD2)||(IRQ_IO==PD3))
// comment or set to 0 if you prefer to use PCINT interrupt
// define and set to 1 if you prefer to use INT interrupt
#define IRQ_INT 1
#endif

#if defined(IRQ_DDR) && defined(IRQ_IO)
/* if the nRF24L01 is initialized with interrupts, watch for nrf_activity to go true
 * once true, check if transmission or reception happened using nrf24_activity(), then nrf24_tx_activity() or nrf24_rx_activity()
 * you can then check is transmission succeeded using nrf24_tx_succeeded()
 * or get the received data using nrf24_rx_data()
 * set it to false after checks are done
 */
extern volatile bool nrf24_flag;
#endif

/* initialize the nRF24L01 */
void nrf24_init();
/* set the 5 bytes TX address
 * it will be used as the destination address of the data to transmit
 * this address will also be used (in RX_ADDR_P0) to receive the ack
 */
void nrf24_set_tx_addr(uint8_t* addr);
/* set the 5 bytes RX address
 * it will be used as the address of the receiving device
 * this address will be used in RX_ADDR_P1
 */
void nrf24_set_rx_addr(uint8_t* addr);
/* set the RF channel to transmit on */
void nrf24_set_rf_channel(uint8_t channel);
/* transmit <payload> with of <length> bytes (max 32 bytes)
 * returns false if input is corrupted or FIFO is already full
 * returns true after payload is written
 */
bool nrf24_transmit(uint8_t* payload, uint8_t length);
/* put device into RX mode or disable it */
void nrf24_enable_rx(bool enable);
/* handle IRQ interrupt */
#if defined(IRQ_IO)
#if defined(IRQ_INT) && (IRQ_INT==1) && (IRQ_IO==PD2) // INT0 interrupt
ISR(INT0_vect);
#elif defined(IRQ_INT) && (IRQ_INT==1) && (IRQ_IO==PD3) // INT1 interrupt
ISR(INT1_vect);
#else // PCINT interrupt
ISR(IRQ_PCINT_vect);
#endif
#endif
/* check the current status of the device
 * return nrf24_activities bits correspoinding to the activies
 * call it after an interrupt
 * puts device back in PRX mode if transmission completed and rx has been enabled
 * puts device back to sleep if transmission completed and rx has not been enabled
 */
enum nrf24_activities {
	TX_SUCCEEDED = (1<<5),
	TX_FAILED = (1<<4),
	RX_RECEIVED = (1<<6),
	RX_AVAILABLE = (1<<3)
};
uint8_t nrf24_activity();
/* writes to received payload in provided <payload> pointer
 * returns size of received data
 * returns 0 if no data is available
 * write maximum <size> in <payload> (max payload size is 32)
 */
uint8_t nrf24_rx_payload(uint8_t* payload, uint8_t size);
/* verify if data is available in the RX FIFO */
bool nrf24_data_available();
/* dump config */
void nrf24_dump();
